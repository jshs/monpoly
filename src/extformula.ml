open Relation
open Predicate
open MFOTL
open Tuple

module Sk = Dllist
module Sj = Dllist

type info  = (int * timestamp * relation) Queue.t
type linfo = {
  mutable llast: Neval.cell;
}
type ainfo = {mutable arel: relation option}
type pinfo = {mutable plast: Neval.cell}
type ninfo = {mutable init: bool}

type agg_info = {op: agg_op; default: cst}

type sinfo = {mutable srel2: relation option;
              saux: Optimized_mtl.msaux}
type uinfo = {mutable ulast: Neval.cell;
              mutable urel2: relation option;
              uaux: Optimized_mtl.muaux}

type comp_one = relation -> relation
type comp_two = relation -> relation -> relation

type extformula =
  | ERel of relation * int
  | EPred of predicate * comp_one * info * int
  | ELet of predicate * comp_one * extformula * extformula * linfo * int
  | ENeg of extformula * int
  | EAnd of comp_two * extformula * extformula * ainfo * int
  | EOr of comp_two * extformula * extformula * ainfo * int
  | EExists of comp_one * extformula * int
  | EAggreg of agg_info * Aggreg.aggregator * extformula * int
  | EAggOnce of agg_info * Aggreg.once_aggregator * extformula * int
  | EPrev of interval * extformula * pinfo * int
  | ENext of interval * extformula * ninfo * int
  | ESince of extformula * extformula * sinfo * int
  | EUntil of extformula * extformula * uinfo * int


  let rec contains_eventually = function
  | ERel           (rel, _)                                      -> false
  | EPred          (p, comp, inf, _)                             -> false
  | ELet           (p, comp, f1, f2, inf, _)                     -> contains_eventually f1 || contains_eventually f2
  | ENeg           (f1, _)                                       -> contains_eventually f1
  | EAnd           (c, f1, f2, ainf, _)                          -> contains_eventually f1 || contains_eventually f2
  | EOr            (c, f1, f2, ainf, _)                          -> contains_eventually f1 || contains_eventually f2
  | EExists        (c, f1, _)                                    -> contains_eventually f1
  | EAggreg        (_inf, _comp, f1, _)                          -> contains_eventually f1
  | EAggOnce       (_inf, _state, f1, _)                         -> contains_eventually f1
  | EPrev          (dt, f1, pinf, _)                             -> contains_eventually f1
  | ENext          (dt, f1, ninf, _)                             -> contains_eventually f1
  | ESince         (f1, f2, sinf, _)                             -> contains_eventually f1 || contains_eventually f2
  | EUntil         (f1, f2, uinf, _)                             -> contains_eventually f1 || contains_eventually f2

(* 
  Print functions used for debugging
 *)  

let prerr_bool b =
  if b then
    prerr_string "true"
  else
    prerr_string "false"

let prerr_ainf str ainf =
  prerr_string str;
  match ainf with
  | None -> prerr_string "None"
  | Some rel -> Relation.prerr_rel "" rel

let prerr_auxel =
  (fun (k,rel) ->
      Printf.eprintf "(%d->" k;
      Relation.prerr_rel "" rel;
      prerr_string ")"
  )
let prerr_sauxel =
  (fun (tsq,rel) ->
      Printf.eprintf "(%s," (MFOTL.string_of_ts tsq);
      Relation.prerr_rel "" rel;
      prerr_string ")"
  )

let prerr_rauxel (j,tsj,rrelsj) =
  Printf.eprintf "(j=%d,tsj=" j;
  MFOTL.prerr_ts tsj;
  prerr_string ",r=";
  Dllist.iter prerr_auxel rrelsj;
  prerr_string "),"


let prerr_aauxel (q,tsq,rel) =
  Printf.eprintf "(%d,%s," q (MFOTL.string_of_ts tsq);
  Relation.prerr_rel "" rel;
  prerr_string ")"

let prerr_inf inf =
  Misc.prerr_queue prerr_aauxel inf

let prerr_predinf str inf =
  prerr_string str;
  prerr_inf inf;
  prerr_newline()

let prerr_linf str inf =
  Printf.eprintf "%s{llast=%s}\n" str (Neval.string_of_cell inf.llast)

let prerr_sinf str inf =
  prerr_string str;
  prerr_ainf "{srel2=" inf.srel2  ;
  prerr_string "; saux=???";
  (* TODO(JS): print saux *)
  prerr_string "}"


let prerr_uinf str inf =
  Printf.eprintf "%s{ulast=%s; " str (Neval.string_of_cell inf.ulast);
  prerr_ainf "urel2=" inf.urel2;
  prerr_string "; uaux=???";
  (* TODO(JS): print uaux *)
  prerr_string "}"

let prerr_extf str ff =
  let prerr_spaces d =
    for i = 1 to d do prerr_string " " done
  in
  let rec prerr_f_rec d f =
    prerr_spaces d;
    (match f with
      | ERel (_, loc) ->
        prerr_string "ERel\n";

      | EPred (p,_,inf,loc) ->
        Predicate.prerr_predicate p;
        prerr_string ": inf=";
        prerr_inf inf;
        prerr_string "\n"

      | _ ->
        (match f with
        | ENeg (f,loc) ->
          prerr_string "NOT\n";
          prerr_f_rec (d+1) f;

        | EExists (_,f,loc) ->
          prerr_string "EXISTS\n";
          prerr_f_rec (d+1) f;

        | EPrev (intv,f,pinf,loc) ->
          prerr_string "PREVIOUS";
          MFOTL.prerr_interval intv;
          prerr_string ": plast=";
          prerr_string (Neval.string_of_cell pinf.plast);
          prerr_string "\n";
          prerr_f_rec (d+1) f

        | ENext (intv,f,ninf,loc) ->
          prerr_string "NEXT";
          MFOTL.prerr_interval intv;
          prerr_string ": init=";
          prerr_bool ninf.init;
          prerr_string "\n";
          prerr_f_rec (d+1) f

        | EAggreg (info,_,f,loc) -> 
          prerr_string (string_of_agg_op info.op);
          prerr_string "_";
          prerr_string (string_of_cst info.default);
          prerr_string "\n";
          prerr_f_rec (d+1) f

        | EAggOnce (info,aggr,f,loc) -> 
            prerr_string (string_of_agg_op info.op);
            prerr_string "ONCE";
            prerr_string "_";
            prerr_string (string_of_cst info.default);
            prerr_string " ";
            Aggreg.prerr_state aggr;
            prerr_string "\n";
            prerr_f_rec (d+1) f

        | _ ->
          (match f with
            | ELet (p,_,f1,f2,linf,loc) ->
              prerr_string "LET: ";
              Predicate.prerr_predicate p;
              prerr_linf " linf=" linf;
              prerr_f_rec (d+1) f1;
              prerr_f_rec (d+1) f2

            | EAnd (_,f1,f2,ainf,loc) ->
              prerr_ainf "AND: ainf=" ainf.arel;
              prerr_string "\n";
              prerr_f_rec (d+1) f1;
              prerr_f_rec (d+1) f2

            | EOr (_,f1,f2,ainf,loc) ->
              prerr_ainf "OR: ainf=" ainf.arel;
              prerr_string "\n";
              prerr_f_rec (d+1) f1;
              prerr_f_rec (d+1) f2

            | ESince (f1,f2,sinf,loc) ->
              prerr_string "SINCE[???]"; (*TODO(JS): print interval*)
              prerr_sinf ": sinf=" sinf;
              prerr_string "\n";
              prerr_f_rec (d+1) f1;
              prerr_f_rec (d+1) f2

            | EUntil (f1,f2,uinf,loc) ->
              prerr_string "UNTIL[???]"; (*TODO(JS): print interval*)
              prerr_uinf ": uinf=" uinf;
              prerr_string "\n";
              prerr_f_rec (d+1) f1;
              prerr_f_rec (d+1) f2

            | _ -> failwith "[prerr_formula] internal error"
          );
        );
    );
  in
  prerr_string str;
  prerr_f_rec 0 ff

let pp_print_comma ppf () =
  Format.pp_print_string ppf ",";
  Format.pp_print_space ppf ()

let pp_print_term ppf t = Format.pp_print_string ppf (Predicate.string_of_term t)

let rec pp_structure ppf ff =
  let open Format in
  let pp_unary name loc f1 =
    fprintf ppf "@[(%d:@;<1 2>%s@;<1 2>%a@,)@]" loc name pp_structure f1
  in
  let pp_binary name loc f1 f2 =
    fprintf ppf "@[(%d:@;<1 2>%s@;<1 2>@[<hv>%a@ %a@]@,)@]"
      loc
      name
      pp_structure f1
      pp_structure f2
  in
  match ff with
  | ERel (rel, loc) ->
      if Relation.is_empty rel then
        fprintf ppf "@[<h>(%d:@ FALSE)@]" loc
      else
        fprintf ppf "@[<h>(%d:@ TRUE)@]" loc
  | EPred ((p, _, args), _, _, loc) ->
      fprintf ppf "@[<h>(%d:@ @[<hov 2>%s(@,%a)@])@]"
        loc
        p
        (pp_print_list ~pp_sep:pp_print_comma pp_print_term) args
  | ELet ((p, _, args), _, f1, f2, _, loc) ->
      fprintf ppf "@[<hv>@[<hov 4>%d: LET@ %s(@,%a)@ =@]@;<1 2>%a@ IN@ %a@]"
        loc
        p
        (pp_print_list ~pp_sep:pp_print_comma pp_print_term) args
        pp_structure f1
        pp_structure f2
  | ENeg (f1, loc) -> pp_unary "NEG" loc f1
  | EAnd (_, f1, f2, _, loc) -> pp_binary "AND" loc f1 f2
  | EOr (_, f1, f2, _, loc) -> pp_binary "OR" loc f1 f2
  | EExists (_, f1, loc) -> pp_unary "EXISTS" loc f1
  | EAggreg (_, _, f1, loc) -> pp_unary "AGGREG" loc f1
  | EAggOnce (_, _, f1, loc) -> pp_unary "AGG ONCE" loc f1
  | EPrev (_, f1, _, loc) -> pp_unary "PREV" loc f1
  | ENext (_, f1, _, loc) -> pp_unary "NEXT" loc f1
  | ESince (f1, f2, _, loc) -> pp_binary "SINCE" loc f1 f2
  | EUntil (f1, f2, _, loc) -> pp_binary "UNTIL" loc f1 f2

let extf_structure ff =
  pp_structure Format.str_formatter ff;
  Format.flush_str_formatter ()
